import { FC } from "react";
import { Post } from "../../../api/types";

type ContentProps = {
  text: string;
}

const Content: FC<ContentProps> = ({ text }) => {
  return (
    <p className="content">{text}</p>
  );
}

export default Content;
