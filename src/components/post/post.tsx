import Content from "./content/content";
import UserHeader from "./user-header/user-header";
import './post.css';
import Image from "./image/image";
import { User } from "../../api/types";
import { Post as PostEntity } from "../../api/types";
import { FC } from "react";
import PostActionsSection from "./actions-section/actions-section";

type PostProps = {
  user: User;
  post: PostEntity
}

const Post: FC<PostProps> = ({ post }) => {
  return (
    <div className="post">
      <UserHeader author={post.author} updatedAt={post.updatedAt} />
      <Content text={post.text} />
      <Image image={post.image} />
      <PostActionsSection post={post} />
    </div>
  );
}

export default Post;
