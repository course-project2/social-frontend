import { FC } from 'react';
import './image.css'

type ImageProps = {
  image: string | undefined;
}

const Image: FC<ImageProps> = ({ image }) => {
  if (image) {
    return (
      <img 
        className="post-image"
        src={image}
        alt='image'
      />
    )
  }
  return null;
}

export default Image;
