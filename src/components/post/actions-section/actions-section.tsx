import './actions-section.css';
import { FC } from 'react'; 
import { useAppDispatch, useAppSelector } from '../../../store/hooks/hooks';
import { $actions } from '../../../store';
import { Post } from '../../../api/types';

type PostActionsSectionProps = {
  post: Post;
}

const PostActionsSection: FC<PostActionsSectionProps> = ({ post }) => {
  const { user } = useAppSelector((state) => state.auth)
  const dispatch = useAppDispatch();

  async function deletePost() {
    dispatch($actions.posts.deletePost(post.id));
  }

  return (
    <div className="post-actions">
      {user?.id === post.authorId ? (
        <button className="open" onClick={deletePost}>
          <i className={'fa fa-trash'}></i>
        </button>
      ) : (
        null
      )}
    </div>
  )
}

export default PostActionsSection;
