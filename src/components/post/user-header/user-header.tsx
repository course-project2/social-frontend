import './user-header.css';
import { FC } from 'react';
import Avatar from './avatar';
import { User } from '../../../api/types';
import humanize from 'humanize-duration';

type UserHeaderProps = {
  author: User;
  updatedAt: Date;
}

const UserHeader: FC<UserHeaderProps> = ({ author, updatedAt }) => {
  const fullName = `${author.firstName} ${author.lastName}`;

  return (
    <div className="user-header">
      <div className="user-info">
        <Avatar firstName={author.firstName} lastName={author.lastName} />
        <a>{fullName}</a>
      </div>
    </div>
  )
}

export default UserHeader;