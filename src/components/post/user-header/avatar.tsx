import { FC } from 'react';
import './avatar.css';

type AvatarProps = {
  firstName: string;
  lastName: string;
}

export const Avatar: FC<AvatarProps> = ({ firstName, lastName }) => {
  const initials = firstName[0] + lastName[0];

  return (
    <div className='avatar'>
      <span>{initials}</span>
    </div>
  )
}

export default Avatar;