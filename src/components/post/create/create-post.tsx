import React, { FC, useRef, useState } from "react";
import { CreatePostDto } from "../../../api/types";
import '../actions-section/actions-section.css'
import './create-post.css';

type CreatePostProps = {
  onSubmit: (dto: CreatePostDto) => Promise<void>;
}

const CreatePost: FC<CreatePostProps> = ({ onSubmit }) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [text, setText] = useState<string>('');

  function handlePostChange(e: React.ChangeEvent<HTMLTextAreaElement>): void {
    setText(e.target.value);
  }

  async function handleSubmit(): Promise<void> {
    const file = inputRef.current?.files?.[0];
    let dto: CreatePostDto = { text };

    if (file) {
      const formData = new FormData();
      formData.append('image', file);
      dto = { ...dto, image: formData };
    }

    setText('');
    await onSubmit(dto);
  }

  return (
    <div className="create-post">
      <textarea
        value={text}
        onChange={handlePostChange}
        placeholder="What's on your mind?"
      />
      <div className="controls">
        <input ref={inputRef} type="file" />
        <button onClick={handleSubmit}>Post</button>
      </div>
    </div>
  );
}

export default CreatePost;