import { FC } from "react";
import { Post as PostEntity } from "../../api/types";
import Post from "./post";

type PostsProps = {
  posts: PostEntity[];
}

const Posts: FC<PostsProps> = ({ posts }) => {
  return (
    <div className="posts">
      {posts.map((post) =>
        <Post key={post.id} user={post.author} post={post} />
      )}
    </div>
  );
}

export default Posts;
