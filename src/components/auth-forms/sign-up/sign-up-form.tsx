import Input from "../input/input";
import '../../post/actions-section/actions-section.css'
import './sign-up-form.css';
import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../store/hooks/hooks";
import { $actions } from "../../../store/ducks";
import { selectors } from "../../../store/auth";
import { Navigate, useNavigate } from "react-router-dom";
import { API_PATHS } from "../../../api/constants";
import { Link } from "react-router-dom";
import { useAuth } from "../../../hooks/use-auth.hook";

export const SignUpForm = () => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [firstName, setFirstName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const dispatch = useAppDispatch();

  async function handleAuthSignUp() {
    await dispatch($actions.auth.signUp({
      email,
      firstName,
      lastName,
      password,
    }));
  }

  return !useAuth() ? (
    <div className="auth-form">
      <Input
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        placeholder={'email'}
        type={'text'}
      />
      <Input
        value={firstName}
        onChange={(e) => setFirstName(e.target.value)}
        placeholder={'firstName'}
        type={'text'}
      />
      <Input
        value={lastName}
        onChange={(e) => setLastName(e.target.value)}
        placeholder={'lastName'}
        type={'text'}
      />
      <Input
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        placeholder={'password'}
        type={'text'}
      />
      <button onClick={handleAuthSignUp}>SignUp</button>
      <Link to='/sign-in'>Sign-in</Link>
    </div>
  ) : (
    <Navigate to='/posts'/>
  )
}

export default SignUpForm;