import Input from "../input/input";
import '../../post/actions-section/actions-section.css'
import './sign-in-form.css';
import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../store/hooks/hooks";
import { $actions } from "../../../store/ducks";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { API_PATHS } from "../../../api/constants";
import { useAuth } from "../../../hooks/use-auth.hook";


export const SignInForm = () => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const { user, accessToken } = useAppSelector((state) => state.auth);

  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  async function handleAuthSignIn() {
    await dispatch($actions.auth.signIn({ email, password }));

    if (user || accessToken) {
      navigate(API_PATHS.POSTS);
    }
  }

  return !useAuth() ? (
    <div className="auth-form">
      <Input
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        placeholder={'email'}
        type={'text'}
      />
      <Input
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        placeholder={'password'}
        type={'text'}
      />
      <button onClick={handleAuthSignIn}>Sign in</button>
      <Link to='/sign-up'>Sign up</Link>
    </div>
  ) : (
    <Navigate to={API_PATHS.POSTS} />
  )
}

export default SignInForm;