import { ComponentProps, FC } from "react";

export interface InputProps extends ComponentProps<"input"> {}

export const Input: FC<InputProps> = ({ placeholder, value, type, onChange }) => {
  return (
    <input
      className="auth-input"
      placeholder={placeholder}
      value={value}
      type={type}
      onChange={onChange}
    />
  )
}

export default Input;