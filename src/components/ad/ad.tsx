import { FC } from 'react';
import './ad.css'

type AdProps = {
  imageUrl: string;
  url: string;
  title: string;
}

const Ad: FC<AdProps> = ({ imageUrl, url, title }) => {
  return (
    <div className="ad">
      <a target="_blank" href={url} rel="noreferrer">
        <img 
          className="ad-img"
          src={imageUrl}
          alt='Ad image.'
        />
      </a>
      <span>{title}</span>
    </div>
  )
}

export default Ad;