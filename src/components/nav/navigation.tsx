import { FC } from "react";
import { Link } from "react-router-dom";
import { useAuth } from "../../hooks/use-auth.hook";
import { $actions } from "../../store";
import { useAppDispatch } from "../../store/hooks/hooks";
import Logo from "./logo";
import './navigation.css';


const Navigation: FC = () => {
  const dispatch = useAppDispatch();

  async function handleLogout(): Promise<void> {
    dispatch($actions.auth.logout());
  }

  return (
    <nav className="navbar">
      <Logo />
      {useAuth() ? (
        <Link
          className='nav-link'
          to='/sign-in'
          onClick={handleLogout}
        >
          Logout
        </Link>
      ) : (
        null
      )}
    </nav>
  )
}

export default Navigation;