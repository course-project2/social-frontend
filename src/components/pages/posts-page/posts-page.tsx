import { FC, useEffect } from "react";
import { Navigate } from "react-router-dom";
import { CreatePostDto } from "../../../api/types";
import { useAuth } from "../../../hooks/use-auth.hook";
import { $actions } from "../../../store/ducks";
import { useAppDispatch, useAppSelector } from "../../../store/hooks/hooks";
import CreatePost from "../../post/create/create-post";
import Posts from "../../post/posts";
import Welcome from "../../welcome/welcome";

export const PostsPage: FC = () => {
  const { posts } = useAppSelector((state) => state.posts);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch($actions.posts.getAllPosts());
  }, []);

  async function createPost(dto: CreatePostDto): Promise<void> {
    dispatch($actions.posts.createPost(dto));
  }

  return useAuth() ? (
    <div className='posts-page'>
      <Welcome />
      <div>
        <CreatePost onSubmit={createPost} />
        <Posts posts={posts} />
      </div>
    </div>
  ) : (
    <Navigate to='/sign-in' />
  )
}

export default PostsPage;