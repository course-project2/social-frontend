import SignUpForm from "../../auth-forms/sign-up/sign-up-form"

export const SignUpPage = () => {
  return (
    <div>
      <SignUpForm />
    </div>
  )
}
