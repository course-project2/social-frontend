import SignInForm from "../../auth-forms/sign-in/sign-in-form"

export const SignInPage = () => {
  return (
    <div>
      <SignInForm />
    </div>
  )
}

export default SignInPage;
