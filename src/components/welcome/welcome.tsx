import { FC } from 'react';
import './welcome.css'


const Welcome: FC = () => {
  return (
    <div className="welcome">
      <h1>Welcome!</h1>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit.
        Deserunt nostrum tempore ullam perspiciatis voluptatem cum quod itaque.
        Praesentium itaque maiores quo incidunt, 
        mollitia sit dolore saepe tempora odio est consequuntur.
      </p>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit.
        Deserunt nostrum tempore ullam perspiciatis voluptatem cum quod itaque.
        Praesentium itaque maiores quo incidunt, 
        mollitia sit dolore saepe tempora odio est consequuntur.
      </p>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit.
        Deserunt nostrum tempore ullam perspiciatis voluptatem cum quod itaque.
        Praesentium itaque maiores quo incidunt, 
        mollitia sit dolore saepe tempora odio est consequuntur.
      </p>
      <a href="#">link</a>
    </div>
  )
}

export default Welcome;
