import { useAppSelector } from "../store/hooks/hooks";

export const useAuth = (): string | null  => {
  const { accessToken } = useAppSelector((state) => state.auth);
  
  return accessToken ? accessToken : null;
}
