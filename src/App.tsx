import './App.css'
import { FC } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navigation from './components/nav/navigation';
import PostsPage from './components/pages/posts-page/posts-page';
import SignInPage from './components/pages/auth-page/sign-in-page';
import { SignUpPage } from './components/pages/auth-page/sign-up-page';


const App: FC = () => {
  return (
    <BrowserRouter>
      <div>
        <Navigation />
        <Routes>
          <Route path='sign-up' element={<SignUpPage />}/>
          <Route path="sign-in" element={<SignInPage />} />
          <Route path="/" element={<PostsPage />} />
          <Route path="posts" element={<PostsPage />}/> 
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
