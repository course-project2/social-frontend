import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { SignInResponseDto, SignUpResponseDto, User } from '../../api/types';
import { $actions } from './thunk';

type AuthState = {
  user?: User;
  accessToken?: string;
}

const initialState: AuthState = {}

export const authReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(
      $actions.signIn.fulfilled.type,
      (state, { payload }: PayloadAction<SignInResponseDto>) => {
        state.accessToken = payload.accessToken;
        state.user = payload.user;
      }
    )
    .addCase(
      $actions.signUp.fulfilled.type,
      (state, { payload }: PayloadAction<SignUpResponseDto>) => {
        state.accessToken = payload.accessToken;
        state.user = payload.user;
      }
    )
    .addCase(
      $actions.logout.fulfilled.type,
      (state) => {
        state.accessToken = undefined;
        state.user = undefined;
      }
    )
});