import { createAsyncThunk } from '@reduxjs/toolkit';
import { API } from '../../api/requests';
import { SignInDto, SignInResponseDto, SignUpDto, SignUpResponseDto } from '../../api/types';
import { CASE_NAMES } from '../constants';

const signIn = createAsyncThunk<SignInResponseDto, SignInDto>(
  CASE_NAMES.SIGN_IN,
  async (dto: SignInDto): Promise<SignInResponseDto> => {
    return API.signIn(dto);
  }
);

const signUp = createAsyncThunk<SignUpResponseDto, SignUpDto>(
  CASE_NAMES.SIGN_UP,
  async (dto: SignUpDto): Promise<SignUpResponseDto> => {
    return API.signUp(dto);
  }
);

const logout = createAsyncThunk(
  CASE_NAMES.LOGOUT,
  async (): Promise<boolean> => {
    return API.logout();
  }
);

export const $actions = {
  signIn,
  signUp,
  logout,
}
