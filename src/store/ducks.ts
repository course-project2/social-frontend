import * as auth from './auth';
import * as posts from './posts';

export const $actions = {
  auth: auth.$actions,
  posts: posts.$actions,
}

