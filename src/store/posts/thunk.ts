import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../../api/requests";
import { CreatePostDto, Post } from '../../api/types';
import { CASE_NAMES } from "../constants";

const getAllPosts = createAsyncThunk<Post[], void>(
  CASE_NAMES.GET_ALL_POSTS,
  async (): Promise<Post[]> => {
    return API.getAllPosts();
  }
);

const createPost = createAsyncThunk<Post, CreatePostDto>(
  CASE_NAMES.CREATE_POST,
  async (dto: CreatePostDto): Promise<Post> => {
    return API.createPost(dto);
  }
);

const deletePost = createAsyncThunk<boolean, string>(
  CASE_NAMES.DELETE_POST,
  async (postId: string): Promise<boolean> => {
    return API.deletePost(postId);
  }
);

export const $actions = {
  getAllPosts,
  createPost,
  deletePost,
}
