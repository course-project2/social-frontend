import { Post } from "../../api/types";
import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { $actions } from "./thunk";

type PostsState = {
  posts: Post[];
  error: string;
}

const initialState: PostsState = {
  posts: [],
  error: '',
}

export const postsReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(
      $actions.getAllPosts.fulfilled.type,
      (state, { payload }: PayloadAction<Post[]> ) => {
        state.posts = payload;
      }
    )
    .addCase(
      $actions.createPost.fulfilled.type,
      (state, { payload }: PayloadAction<Post>) => {
        state.posts.unshift(payload);
      }
    )
    .addCase(
      $actions.deletePost.fulfilled.type,
      (state, { payload }: PayloadAction<string>) => {
        state.posts = state.posts.filter((post) => post.id !== payload);
      }
    )
})
