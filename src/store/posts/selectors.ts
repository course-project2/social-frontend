import { RootState } from "../config";

export const selectors = {
  posts: (state: RootState) => state.posts,
};
