// USER
export type User = {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  currentRefreshToken: string;
  createdAt: string;
  updatedAt: string;
}

// POST
export type Post = {
  id: string;
  text: string;
  image?: string;
  authorId: string;
  author: User;
  createdAt: Date;
  updatedAt: Date;
}

export type CreatePostDto = {
  text?: string;
  image?: FormData;
}

export type UpdatePostDto = {
  text?: string;
  image?: FormData;
}

// AUTH
export type SignUpDto = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export type SignInDto = {
  email: string;
  password: string;
}

export type SignInResponseDto = {
  accessToken: string;
  refreshToken: string;
  user: User;
}

export type SignUpResponseDto = {
  accessToken: string;
  refreshToken: string;
  user: User;
}