import { httpClient } from "../config"
import { API_PATHS } from "../constants"
import { CreatePostDto, Post, SignInDto, SignInResponseDto, SignUpDto, SignUpResponseDto } from "../types";

// POSTS
const getAllPosts = async () => {
  const response = await httpClient.get(API_PATHS.POSTS);
  const posts = response.data;

  return posts;
}

const deletePost = async (postId: string): Promise<boolean> => {
  const response = await httpClient.delete(API_PATHS.POSTS + '/' + postId);

  return response.data
}

const createPost = async (dto: CreatePostDto): Promise<Post> => {
  const headers = {
    'Content-Type': 'multipart/form-data',
  }

  const user = localStorage.getItem('user');

  const response = await httpClient.post(
    API_PATHS.POSTS,
    { text: dto.text },
  );
  
  let post = response.data;

  if (dto.image) {
    const uploadResponse = await httpClient.patch(
      `${API_PATHS.POSTS}/${post.id}`,
      dto.image,
      { headers },
    );

    post = { ...post, ...uploadResponse.data };
  }

  post.author = await getUserById(post.authorId);
  
  return post;
}

// USERS
const getUserById = async (id: string) => {
  const response = await httpClient.get(API_PATHS.USERS + '/' + id);
  return response.data;
}

// AUTH
const signIn = async (dto: SignInDto): Promise<SignInResponseDto> => {
  const response = await httpClient.post(API_PATHS.SIGN_IN, dto);
  const { user, accessToken } = response.data;

  if (response.data) {
    localStorage.setItem('user', user);
    localStorage.setItem('accessToken', accessToken);
  }
  
  return response.data;
}

const signUp = async (dto: SignUpDto): Promise<SignUpResponseDto> => {
  const response = await httpClient.post(
    API_PATHS.SIGN_UP,
    JSON.stringify(dto)
  );

  const { user, accessToken } = response.data;

  if (response.data) {
    localStorage.setItem('user', user);
    localStorage.setItem('accessToken', accessToken);
  }

  return response.data;
}

const logout = async (): Promise<boolean> => {
  const response = await httpClient.post(API_PATHS.LOGOUT);

  if (response.data) {
    localStorage.removeItem('user');
    localStorage.removeItem('accessToken'); 
  }

  return response.data;
}

export const API = {
  getAllPosts,
  createPost,
  deletePost,
  getUserById,
  signIn,
  signUp,
  logout,
}

