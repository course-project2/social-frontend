import axios, { AxiosRequestConfig } from "axios";
import { store } from "../../store/config";
import { API_CONSTANTS } from "../constants";

const config: AxiosRequestConfig = {
  baseURL: API_CONSTANTS.BASE_URL,
  headers: API_CONSTANTS.HEADERS,
}

export const httpClient = axios.create(config);

httpClient.interceptors.request.use((config) => {
  const token = store.getState().auth.accessToken;
  if (token) {
    config.headers!.Authorization = `Bearer ${token}`;
  }
  return config;
});
