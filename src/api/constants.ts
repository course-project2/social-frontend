export const API_CONSTANTS = {
  BASE_URL: 'https://social-posts-backend.herokuapp.com',
  HEADERS: {
    "access-control-allow-origin": "*",
    "Content-Type": "application/json",
  },
}

export const enum API_PATHS {
  POSTS = '/posts',
  USERS = '/users',
  SIGN_IN = '/auth/signIn',
  SIGN_UP = '/auth/signUp',
  LOGOUT = '/auth/logout',
}
